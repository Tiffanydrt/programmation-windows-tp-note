
Ce TP en C# et plus précisément en .NET FrameWork contient un solution appelée BasicCounterSolution. Il a but pour de manipuler un compteur et est réalisé avec une interface graphique WinForm. 


## Diagramme UML

![UML diagramm](img/uml.png)

## Comment utiliser le projet ? 

**Lancer les tests unitaires**
Aller dans "Test" "Exécuter" "Tous les tests"

**Compiler le projet**

"Démarrer" 

## Projets

BasicCounterSolution est composé de deux projets:

### BasicCounterConsole
Permet de manipuler un compteur. 
Ce projet contient une classe appelée BasicCounterMyClass.

Cette classe est livrée avec 5 fonctions :
-Un constructeur
-Une fonction Incremente de type int qui permet d'incrémenter le compteur.
-Une fonction Decremente de type int qui permet de décrémenter le compteur.
-Une fonction RAZ de type int qui permet de remettre le compteur à 0.
-Un Getter de l'attribut "value". 


### TestBasicCounter
Tests unitaires des fonctions de la class BasicCounterMyClass.



