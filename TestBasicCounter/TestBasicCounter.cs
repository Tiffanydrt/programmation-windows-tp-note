﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BasicCounterClass;

namespace TestBasicCounter
{
    [TestClass]
    public class TestBasicCounter
    {
        [TestMethod]
        public void TestIncremente()
        {
            BasicCounterMyClass compteur1= new BasicCounterMyClass(0);
            Assert.AreEqual(1, compteur1.Incremente());
            Assert.AreEqual(2, compteur1.Incremente());

        }

        [TestMethod]
        public void TestDecremente()
        {
            BasicCounterMyClass compteur1 = new BasicCounterMyClass(1);
            Assert.AreEqual(0, compteur1.Decremente());

        }

        [TestMethod]
        public void TestRAZ()
        {
            BasicCounterMyClass compteur1 = new BasicCounterMyClass(1);
            Assert.AreEqual(0, compteur1.RAZ());

        }


    }
}
