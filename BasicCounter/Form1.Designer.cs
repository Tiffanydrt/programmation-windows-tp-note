﻿namespace BasicCounter
{
    partial class Form1
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.boutonMoins = new System.Windows.Forms.Button();
            this.boutonPlus = new System.Windows.Forms.Button();
            this.boutonRAZ = new System.Windows.Forms.Button();
            this.labelTotal = new System.Windows.Forms.Label();
            this.labelChiffre = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // boutonMoins
            // 
            this.boutonMoins.Location = new System.Drawing.Point(201, 171);
            this.boutonMoins.Name = "boutonMoins";
            this.boutonMoins.Size = new System.Drawing.Size(75, 23);
            this.boutonMoins.TabIndex = 0;
            this.boutonMoins.Text = "-";
            this.boutonMoins.UseVisualStyleBackColor = true;
            this.boutonMoins.Click += new System.EventHandler(this.BoutonMoins_Click);
            // 
            // boutonPlus
            // 
            this.boutonPlus.Location = new System.Drawing.Point(427, 171);
            this.boutonPlus.Name = "boutonPlus";
            this.boutonPlus.Size = new System.Drawing.Size(75, 23);
            this.boutonPlus.TabIndex = 1;
            this.boutonPlus.Text = "+";
            this.boutonPlus.UseVisualStyleBackColor = true;
            this.boutonPlus.Click += new System.EventHandler(this.BoutonPlus_Click);
            // 
            // boutonRAZ
            // 
            this.boutonRAZ.Location = new System.Drawing.Point(310, 222);
            this.boutonRAZ.Name = "boutonRAZ";
            this.boutonRAZ.Size = new System.Drawing.Size(75, 23);
            this.boutonRAZ.TabIndex = 2;
            this.boutonRAZ.Text = "RAZ";
            this.boutonRAZ.UseVisualStyleBackColor = true;
            this.boutonRAZ.Click += new System.EventHandler(this.BoutonRAZ_Click);
            // 
            // labelTotal
            // 
            this.labelTotal.AutoSize = true;
            this.labelTotal.Location = new System.Drawing.Point(345, 105);
            this.labelTotal.Name = "labelTotal";
            this.labelTotal.Size = new System.Drawing.Size(31, 13);
            this.labelTotal.TabIndex = 3;
            this.labelTotal.Text = "Total";
            this.labelTotal.Click += new System.EventHandler(this.LabelTotal_Click);
            // 
            // labelChiffre
            // 
            this.labelChiffre.AutoSize = true;
            this.labelChiffre.Location = new System.Drawing.Point(345, 135);
            this.labelChiffre.Name = "labelChiffre";
            this.labelChiffre.Size = new System.Drawing.Size(13, 13);
            this.labelChiffre.TabIndex = 4;
            this.labelChiffre.Text = "0";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.labelChiffre);
            this.Controls.Add(this.labelTotal);
            this.Controls.Add(this.boutonRAZ);
            this.Controls.Add(this.boutonPlus);
            this.Controls.Add(this.boutonMoins);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button boutonMoins;
        private System.Windows.Forms.Button boutonPlus;
        private System.Windows.Forms.Button boutonRAZ;
        private System.Windows.Forms.Label labelTotal;
        private System.Windows.Forms.Label labelChiffre;
    }
}

