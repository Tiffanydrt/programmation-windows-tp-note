﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BasicCounterClass;


namespace BasicCounter
{
    public partial class Form1 : Form
    { BasicCounterMyClass compteur;
        public Form1()
        {
            InitializeComponent();
            this.compteur=new BasicCounterMyClass(0);
        }

        private void BoutonPlus_Click(object sender, EventArgs e)
        {
            labelChiffre.Text = String.Format(this.compteur.Incremente().ToString());
        }

        private void BoutonMoins_Click(object sender, EventArgs e)
        {
            labelChiffre.Text = String.Format(this.compteur.Decremente().ToString());
        }

       private void BoutonRAZ_Click(object sender, EventArgs e)
        {
            labelChiffre.Text = String.Format(this.compteur.RAZ().ToString());
        }

        private void LabelTotal_Click(object sender, EventArgs e)
        {

        }
    }
}
