﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;



namespace BasicCounterClass
{
    public class BasicCounterMyClass

    {   /*Pour que l'attribut ne soit accessible que dans ma classe*/
        private int value;


        public BasicCounterMyClass(int value)
        {
            this.value = value;

        }

        public int Incremente()
        {
            value = value + 1;

            return value;
        }

        public int Decremente()
        {
            value = value - 1;

            if (value < 0)
            {
                value = 0;
            }

            return value;

        }

        public int RAZ()
        {
            value = 0;
            return value;
        }


        /*J'ai fait un getter pour si jamais on développe des nouvelles
        * fonctions plus tard qui en ont besoin*/
        public int GetValue()
        {
            return value;
        }

    }
}
